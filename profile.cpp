#include "profile.h"
#include "tlv.h"

Profile::Profile(QWidget *parent, int profileNum) : QWidget(parent) , profileNumber(profileNum)
{

    sendButton = new QPushButton("Send to Device",this);
    QGridLayout *mainLayout = new QGridLayout;

    for (int i=0; i<maxTimeValuePairs; i++)
    {
        ProfileTimeValueBox *newBox = new ProfileTimeValueBox(this);
        boxList.append(newBox);
        mainLayout->addWidget(boxList.at(i),0,i,Qt::AlignHCenter);
    }
    mainLayout->addWidget(sendButton,1,0);

    setLayout(mainLayout);
 /*   label->setGeometry(10,255,600,10);
    label->show();
*/
    connect(sendButton,SIGNAL(clicked()),this,SLOT(on_sendButton_clicked()));
}

void Profile::on_sendButton_clicked(void)
{
    Message msgToSend;
    tlvPutProfile putProfileCommand(profileNumber);
    msgToSend.appendTlv(putProfileCommand.getRawData());
    tlvValueTime profile;

    for (int i=0; i<maxTimeValuePairs; i++)
    {
        profile.addValueTimePair(boxList.at(i)->getValue(),boxList.at(i)->getTime());
    }
    msgToSend.appendTlv(profile.getRawData());
//    label->setText(msgToSend.getRawData().toHex());
    emit msgReadyToSend(msgToSend);
}


ProfileTimeValueBox::ProfileTimeValueBox(QWidget *parent) : QWidget(parent)
{
    spinBox->setDecimals(1);
    spinBox->setMaximumWidth(50);
    spinBox->setAccelerated(true);
    spinBox->setMaximum(100);

    slider->setMinimumHeight(150);
    slider->setMaximum(1000);
    slider->setSingleStep(10);

    timeEdit->setMaximumWidth(80);
    timeEdit->setDisplayFormat("HH:mm:ss");

    QGridLayout *layout = new QGridLayout();
    layout->addWidget(spinBox,0,0,Qt::AlignHCenter);
    layout->addWidget(slider,1,0,Qt::AlignHCenter);
    layout->addWidget(timeEdit,2,0,Qt::AlignHCenter);
    setLayout(layout);

    connect(slider, SIGNAL(valueChanged(int)), this, SLOT(sliderToSpinBox()));
    connect(spinBox, SIGNAL(valueChanged(double)), this, SLOT(spinBoxToSlider()));
}

QTime ProfileTimeValueBox::getTime(void)
{
    return timeEdit->time();
}

int ProfileTimeValueBox::getValue(void)
{
    return slider->value();
}

void ProfileTimeValueBox::sliderToSpinBox(void)
{
    spinBox->setValue(slider->value()/10.0);
}

void ProfileTimeValueBox::spinBoxToSlider(void)
{
    slider->setValue(static_cast<int>(spinBox->value()*10));
}
