#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QTimer>
#include "profile.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void connectSerialPort(void);
    void disconnectSerialPort(void);
    void updateCurrentTime(void);
    void readSerailPortData(void);
    void sendMessage(Message msg);

    void on_connectButton_clicked();

    void on_setTimeButton_clicked();

    void on_syncButton_clicked();

    void on_writeFlashButton_clicked();

protected:
    bool eventFilter(QObject *target, QEvent *event);

private:
    Ui::MainWindow *ui;
    QList<QWidget*> profileTabsPtrs;
    QList<Profile*> profilePtrs;

    //User added
    const int profileMaxCount = 8;

    QSerialPort *comPort;
    void comPortEnumerate(void);
    void setTimeOnDevice(QTime time);
};

#endif // MAINWINDOW_H
