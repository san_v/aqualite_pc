#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tlv.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    for (int i=0; i<profileMaxCount; i++)
    {
        QWidget *ptr = new QWidget(ui->tabWidget);
        profileTabsPtrs.append(ptr);
        QString tabName = "Profile " + QString::number(i+1);
        profileTabsPtrs.at(i)->setAutoFillBackground(true);
        ui->tabWidget->addTab(profileTabsPtrs.at(i),tabName);
        profileTabsPtrs.at(i)->setDisabled(true);

        Profile *ptr1 = new Profile(profileTabsPtrs.at(i),i);
        profilePtrs.append(ptr1);        
        ptr1->show();
        connect(ptr1,SIGNAL(msgReadyToSend(Message)),this,SLOT(sendMessage(Message)));
    }

    //Setup filter to catch comboBoxClick
    ui->comPortSelectionComboBox->installEventFilter(this);

    comPort = new QSerialPort(this);
    connect(comPort,SIGNAL(readyRead()),this,SLOT(readSerailPortData()));

    //Create timer for periodical polling for current system time (each 100ms)
    QTimer *clockTimer = new QTimer(this);
    connect(clockTimer,SIGNAL(timeout()),this,SLOT(updateCurrentTime()));
    clockTimer->start(100);
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::eventFilter(QObject *target, QEvent *event)
{
    if (target == ui->comPortSelectionComboBox) {
        if (event->type() == QEvent::KeyPress || event->type() == QEvent::MouseButtonPress)
        {
            comPortEnumerate();
        }
    }
  return false;
}

void MainWindow::comPortEnumerate(void)
{
    QList<QSerialPortInfo> comPortList = QSerialPortInfo::availablePorts();
    ui->comPortSelectionComboBox->clear();
    if (comPortList.size() > 0)
    {
        for (int i=0; i<comPortList.size(); i++)
        {
            ui->comPortSelectionComboBox->addItem(comPortList.at(i).portName());
        }
        ui->connectButton->setEnabled(true);
    }
    else ui->comPortSelectionComboBox->addItem("Select COM port");
}

void MainWindow::connectSerialPort(void)
{
    comPort->setPortName(ui->comPortSelectionComboBox->currentText());
    if (comPort->open(QIODevice::ReadWrite)) {
        QString printable = QString("Connected to %1").arg(ui->comPortSelectionComboBox->currentText());
        ui->statusString->setText(printable);
        ui->connectButton->setText("Disconnect");
        ui->comPortSelectionComboBox->setDisabled(true);
        ui->timeSettings->setEnabled(true);

        for (int i=0;i<profileMaxCount;i++)
        {
            profileTabsPtrs.at(i)->setEnabled(true);
        }

    } else {
        QString printable = QString("Couldn't connect to %1").arg(ui->comPortSelectionComboBox->currentText());
        ui->statusString->setText(printable);
    }
}

void MainWindow::disconnectSerialPort(void)
{
    if (comPort->isOpen())
        comPort->close();
}

void MainWindow::readSerailPortData(void)
{
    QByteArray data = comPort->readAll();
    char *num = data.data();
    QTime timeOnDevice;
    timeOnDevice.setHMS(*num,*(num+1),*(num+2));
//    QString s;
//    s += QString::number(*(num),16) + ":" + QString::number(*(num+1),16) + ":" + QString::number(*(num+2),16);
    ui->deviceTimeValue->setText(timeOnDevice.toString("hh:mm:ss"));
}


void MainWindow::on_connectButton_clicked()
{
    if (ui->connectButton->text() == "Connect")
     {
         disconnectSerialPort();
         connectSerialPort();
     }
     else
     {
         disconnectSerialPort();
         ui->comPortSelectionComboBox->clear();
         ui->comPortSelectionComboBox->addItem("Select COM port");
         ui->comPortSelectionComboBox->setEnabled(true);
         ui->connectButton->setText("Connect");
         ui->connectButton->setDisabled(true);
         ui->statusString->setText("No device connected");
         ui->timeSettings->setDisabled(true);

         for (int i=0;i<profileMaxCount;i++)
         {
             profileTabsPtrs.at(i)->setDisabled(true);
         }
     }

}

void MainWindow::updateCurrentTime(void)
{
    QTime currentTimer = QTime::currentTime();
    ui->pcTimeValue->setText(currentTimer.toString("hh:mm:ss"));
}

void MainWindow::setTimeOnDevice(QTime time)
{
    uint32_t timestamp = time.hour()*3600 + time.minute()*60 + time.second();

    tlvTime timeToSend(timestamp);
    Message msgToSend;
    msgToSend.appendTlv(timeToSend.getRawData());
    comPort->write(msgToSend.getRawData());
}

void MainWindow::on_setTimeButton_clicked()
{
    QTime time = ui->timeEdit->time();
    setTimeOnDevice(time);
}


void MainWindow::on_syncButton_clicked()
{
    QTime time = QTime::currentTime();
    setTimeOnDevice(time);
}

void MainWindow::sendMessage(Message msg)
{
    comPort->write(msg.getRawData());
}

void MainWindow::on_writeFlashButton_clicked()
{
    Message msgToSend;
    tlvWriteFlash writeTlv;
    msgToSend.appendTlv(writeTlv.getRawData());
//    ui->label->setText(msgToSend.getRawData().toHex());
    comPort->write(msgToSend.getRawData());
}
