#-------------------------------------------------
#
# Project created by QtCreator 2016-02-11T21:14:52
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = aquaLite
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    profile.cpp \
    tlv.cpp

HEADERS  += mainwindow.h \
    profile.h \
    tlv.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -std=gnu++11
