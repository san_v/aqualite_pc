#ifndef TLV_H
#define TLV_H

#include <cstdint>
#include <QByteArray>
#include <QtEndian>
#include <QList>
#include <QTime>

class Message
{
private:
    const char delimiter[2] = {0xAB,0xD5}; //delimiter 0xABD5
    uint16_t totalMessageLen = 0;
    QByteArray rawData;
public:
    Message();
    QByteArray getRawData(void);
    void appendTlv(QByteArray tlv);
};

class tlv
{
public:
    tlv();
//    uint16_t getType(void) {return type;}
//    uint16_t getLength(void) {return length;}
    QByteArray getRawHeader(void);
protected:
    uint16_t type;
    uint16_t length;
//    uint8_t *value;
};


class tlvTime: public tlv
{
private:
    uint32_t value;
public:
    tlvTime(uint32_t timestamp);
    uint32_t getTimestamp(void);
    QByteArray getRawData(void);
};

class tlvPutProfile: public tlv
{
private:
    uint16_t value;
public:
    tlvPutProfile(uint16_t profileNumber);
    QByteArray getRawData(void);
};

class tlvValueTime: public tlv
{
private:
    QList<uint16_t> value;
    QList<uint32_t> timestamp;
    int size; //Number of Value-Time pairs
public:
    tlvValueTime(void);
    void addValueTimePair(uint16_t newValue, QTime newTime);
    QByteArray getRawData(void);
};

class tlvWriteFlash: public tlv
{
private:
    uint16_t value;
public:
    tlvWriteFlash(void);
    QByteArray getRawData(void);
};

#endif // TLV_H
