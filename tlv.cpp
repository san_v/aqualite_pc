#include "tlv.h"

Message::Message()
{
    rawData.append(delimiter);  //Write delmiter
    rawData.resize(sizeof(delimiter)+sizeof(totalMessageLen)); //Add bytes for Length field
}


void Message::appendTlv(QByteArray tlv)
{
    rawData.append(tlv);
    totalMessageLen += tlv.size();
    uint16_t lengthBE = qToBigEndian(totalMessageLen);
    QByteArray tmp = QByteArray(reinterpret_cast<char*>(&lengthBE),sizeof(totalMessageLen));
    rawData.replace(2,sizeof(totalMessageLen),tmp);
}


QByteArray Message::getRawData(void)
{
    return rawData;
}

tlv::tlv()
{

}

QByteArray tlv::getRawHeader(void)
{
    auto typeBE = qToBigEndian(type);
    auto lengthBE = qToBigEndian(length);
    QByteArray rawHeader;
    rawHeader.append(reinterpret_cast<char*>(&typeBE),sizeof(typeBE));
    rawHeader.append(reinterpret_cast<char*>(&lengthBE),sizeof(lengthBE));
    return rawHeader;
}

tlvTime::tlvTime(uint32_t timestamp)
{
    type = 0x0020;
    length = 0x0004;
    value = timestamp;
}

uint32_t tlvTime::getTimestamp(void)
{
    return value;
}

QByteArray tlvTime::getRawData(void)
{
    QByteArray rawData(getRawHeader());
    auto timestampBE = qToBigEndian(value);
    rawData.append(reinterpret_cast<char*>(&timestampBE),sizeof(timestampBE));
    return rawData;
}

tlvPutProfile::tlvPutProfile(uint16_t profileNumber)
{
    type = 0x0101;
    length = 0x0002;
    value = profileNumber;
}

QByteArray tlvPutProfile::getRawData(void)
{
    QByteArray rawData(getRawHeader());
    auto profileBE = qToBigEndian(value);
    rawData.append(reinterpret_cast<char*>(&profileBE),sizeof(profileBE));
    return rawData;
}

tlvValueTime::tlvValueTime(void)
{
    type = 0x0030;
    length = 0; //Will be re-calculated while new Value-Time pairs will be added
    size = 0;
}

void tlvValueTime::addValueTimePair(uint16_t newValue, QTime newTime)
{
    size++;
    value.append(newValue);
    timestamp.append(newTime.hour()*3600 + newTime.minute()*60 + newTime.second());
    length = size*(sizeof(value.at(0))+sizeof(timestamp.at(0)));
}

QByteArray tlvValueTime::getRawData(void)
{
    QByteArray rawData(getRawHeader());
    auto valueBE = value.at(0);
    auto timestampBE = timestamp.at(0);
    for (int i=0; i<size; i++)
    {
        valueBE = qToBigEndian(value.at(i));
        timestampBE = qToBigEndian(timestamp.at(i));
        rawData.append(reinterpret_cast<char*>(&valueBE),sizeof(valueBE));
        rawData.append(reinterpret_cast<char*>(&timestampBE),sizeof(timestampBE));
    }
    return rawData;
}

tlvWriteFlash::tlvWriteFlash(void)
{
    //Create TLV with 0xffff payload which means "Write all profiles"
    type = 0x0103;
    length = 0x0002;
    value = 0xffff;
}

QByteArray tlvWriteFlash::getRawData(void)
{
    QByteArray rawData(getRawHeader());
    auto valueBE = qToBigEndian(value);
    rawData.append(reinterpret_cast<char*>(&valueBE),sizeof(valueBE));
    return rawData;
}
