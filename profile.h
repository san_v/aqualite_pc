#ifndef PROFILE_H
#define PROFILE_H

#include <QWidget>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QSlider>
#include <QTimeEdit>
#include <QList>
#include <QDoubleSpinBox>
#include <QPushButton>
#include "tlv.h"

class ProfileTimeValueBox : public QWidget
{
    Q_OBJECT
public:
    explicit ProfileTimeValueBox(QWidget *parent = 0);
    QTime getTime(void);
    int getValue(void);

//signals:

public slots:
    void sliderToSpinBox(void);
    void spinBoxToSlider(void);

private:
    QDoubleSpinBox *spinBox = new QDoubleSpinBox(this);
    QSlider *slider = new QSlider(Qt::Vertical,this);
    QTimeEdit *timeEdit = new QTimeEdit(this);
};

class Profile : public QWidget
{
    Q_OBJECT
public:
    explicit Profile(QWidget *parent, int profileNum);

signals:
    void msgReadyToSend(Message message);

public slots:
    void on_sendButton_clicked(void);

private:
    const int maxTimeValuePairs = 6;
    const int profileNumber;
    QList<ProfileTimeValueBox*> boxList;
    QPushButton *sendButton;
//    QLabel *label = new QLabel(this);

};


#endif // PROFILE_H
